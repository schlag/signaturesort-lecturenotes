\documentclass[11pt]{article}


% Für deutsche Trennungen, Datumsangaben etc.:
%\usepackage{german} % DON'T use if writing in English
% Um diakritische Zeichen (Umlaute etc.) direkt (=im Editor) einzugeben:
%\usepackage[utf8]{inputenc} % evtl. utf8 je nach System/Editor ERSETZEN, z.B. zu latin1
\usepackage{a4}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{graphicx} % include graphics with \includegraphics{xyz.pdf}
\usepackage[algoruled]{algorithm2e} % \begin{algorithm} ... \end{algorithm}
\usepackage{enumitem}
\usepackage[numbers]{natbib}

\usepackage{amsthm}
\newtheorem{thm}{Theorem}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{defi}{Definition}
\newtheorem{obs}{Observation}
\newtheorem{prob}{Problem}
\newtheorem{ex}{Example}

\DeclareMathOperator*{\argmax}{argmax}
\DeclareMathOperator*{\argmin}{argmin}

\pagestyle{plain}
%\parindent 0pt
%\parskip 11pt

\author{Schriftf\"uhrer: Sebastian Schlag}
\title{Fortgeschrittene Datenstrukturen --- Vorlesung 6}
\date{24.11.2011}

\begin{document}
\maketitle
\nocite{*}
\section{Integer Sorting} % (fold)
\label{sec:integer_sorting}

%In this lecture we consider the problem of sorting n $w$-bit integers. We assume that the word size of our computer is in $\Omega(w)$, so we can address and manipulate a single key in $\mathcal{O}(1)$ time. Thus we are working in the RAM model.
The sorting problem of sorting $n$ elements has a tight $\Theta(n \lg n)$ bound in the comparison model. In this lecture, we will cover a special case of the general sorting problem, namely \textit{integer sorting}, in which we have to sort $n$ $w$-bit integers. 

By working in the RAM model, we assume that the word size of our computer is in $\Omega(w)$, so we can address and manipulate a single key in $\mathcal{O}(1)$ time.We will also consider shorter keys of $b\leq w$ bits. In general, we use the notation \texttt{SORT($n$,$w$)} for the complexity of sorting $n$ $w$-bit integers

Section \ref{sub:overview_what_we_already_know} provides an overview of different integer sorting algorithms. Section \ref{sub:signature_sort} will then cover \textit{signature sort}, which runs in $\mathcal{O}(n)$ expected time for  $w \geq \lg^{2+\varepsilon}n \lg \lg n $.

\subsection{Overview} % (fold)
\label{sub:overview_what_we_already_know}
As figure \ref{fig:sorts} shows it is still an open question if there is a linear time sorting algorithm for $w=\omega(\lg n)$ and $w=o(\lg^{2+\varepsilon} n)$. Currently, the best known algorithm covering this range is due to \citet{Thorup2002}.

 \begin{figure}[h!tbp]
 	\centering
 	\includegraphics[width=0.6\textwidth]{pics/sorts.pdf}
 	\caption[]{Signature sort, radix sort and v.E.B. sort and their corresponding runtime in respect to n and $w$}
 	\label{fig:sorts}
 \end{figure}
 Following is a list of results on the problem of integer sorting:
\begin{itemize}
	\item \textit{Counting sort} runs in $\textrm{SORT}(n,w)=\mathcal{O}(n + 2^w)$, which is linear for $w \leq \lg n$.
		\item \textit{Radix sort} runs in $\textrm{SORT}(n,w)=\mathcal{O}\left(n \cdot \frac{w}{\lg n}\right)$, which is linear for $w=\mathcal{O}(\lg n)$
	\item \textit{van Emde Boas sort} runs in $\textrm{SORT}(n,w)=\mathcal{O}(n \lg w)$, which is $\mathcal{O}(n~\lg\lg~n)$ for $w=\lg^{\mathcal{O}(1)}n$ and can be improved to $\mathcal{O}\left(n \lg \frac{w}{\lg n}\right)$ (see \citep{KirkpatrickR84}).
	\item \citet{Thorup2002} present a randomized algorithm for sorting n integers in $\mathcal{O}(n\sqrt{\lg \lg n})$ expected time.
\end{itemize}

%In the next subsection, we will introduce \textit{signature sort} that runs in $\mathcal{O}(n)$ expected time for  $w \geq \lg^{2+\varepsilon}n \lg \lg n $. This implies $\textrm{SORT}(n,w)=\mathcal{O}(n \lg\lg n)$ independent of $w$ (see  \citep{Andersson1995}).

% subsection overview_what_we_already_know (end)
\subsection{Signature Sort} % (fold)
\label{sub:signature_sort}
The basic idea of signature sort \citep{Andersson1995} is to break each integer key into chunks and (similar to fusion trees) reduce the bit-length of each chunk such that the resulting integers can be sorted in $\mathcal{O}(n)$ time using \textit{packed sorting} (which is subject of the next lecture).


Let $X_{1},\dotsc,X_{n}$ denote the set of integer keys to be sorted and assume $w \geq \lg^{2+\varepsilon}n \lg \lg n $.
We begin by conceptually partitioning each key in \textit{q chunks} of size $\frac{w}{q}$ (see fig. \ref{fig:keyandchunks}).
During the analysis we will impose \textit{two} requirements on \textit{q}. At the end of this subsection we will then prove that \textit{q} can be chosen such that both requirements are fulfilled.

 \begin{figure}[h!tbp]
 	\centering
 	\includegraphics[width=0.2\textwidth]{pics/fig1.pdf}
 	\caption[]{Each $w$-bit key $X_{i}$ is broken into \textit{q} chunks of size $\frac{w}{q}$.}
 	\label{fig:keyandchunks}
 \end{figure}


%In order to create the length-reduced signatures, we randomly choose a hash function $h_{a}$ from a class of 2-universal hash functions $\mathcal{H}$ and use it to hash each chunk $x_{i,j}$ to create its unique signature $h_{a}(x_{i,j})$. 
In order to be able to sort these chunks efficiently, we use a universal hash function $h_{a}$ that maps each chunk to a \textit{unique} hash-value  that is significantly smaller than the original chunk. The hash-value $h_{a}(x_{i,j})$ of a chunk $x_{i,j}$ will be called the \textit{signature} of $x_{i,j}$. As key $X_{i}$ consists of chunks $x_{i,1},\dotsc,x_{i,q}$, we define the concatenated signature $h_{a}(X_{i})$ of $X_{i}$ as the integer obtained by concatenating the signatures $h_{a}(x_{i,1}),\dotsc,h_{a}(x_{i,q})$ (see fig.\ref{fig:hash}).

 \begin{figure}[h!tbp]
 	\centering
 	\includegraphics[width=0.49\textwidth]{pics/fig2.pdf}
 	\caption[]{Using $h_{a}$ to create the signature $h_{a}(X_{i})$ of key $X_{i}$ and thereby reducing the length of each key from $w$ bits to $\Theta(q\lg n)$ bits.}
 	\label{fig:hash}
 \end{figure}

Because we want the signatures to be \textit{unique}, our hash function has to operate \textit{injectively} on the set of all $n\cdot q$ chunks occuring in the input keys. Furthermore, we require the length of signature $h_{a}(x_{i,j})$ to be in $\Theta(\lg n)$.

The following class $\mathcal{H}_{k,l}$ of 2-universal hash functions \citep{Dietzfelbinger:1997:RRA:264631.264633} satisfies these requiremens:
\begin{align*}
\mathcal{H}_{k,l} = \{h_{a}|0 &\le a \le 2^k \wedge a \textrm{ is odd}\} \textrm{ where } h_{a} \textrm{ is defined by: } \\
&h_{a} \colon \{0,\dotsc, 2^k-1\} \to \{0,\dotsc,2^l-1\} \\
&h_{a}(x) = (ax~\mod~2^k)~\textrm{div}2^{k-l} 
\end{align*}
 \citet{Dietzfelbinger:1997:RRA:264631.264633} prove (see Lemma 2.6) that given integers $k$ and $l$ with $1 \leq l\leq k$ and a set $S$ of integers in the range $\{0,\dotsc,2^k-1\}$: If $h_{a} \in H_{k,l}$ is chosen at random, then $$\textrm{Prob}(h_{a}\textrm{ is injective on }S) \geq 1-\frac{|S|^2}{2^l}.$$

Recall that in our case $S=n\cdot q$ is the set of all chunks occuring in the input keys. Without loss of generality we can assume that $q \leq n$ and therefore $|S| \leq n^2$. Thus we can assure that $$\textrm{Prob}(h_{a}\textrm{ is \textbf{not} injective on S})\leq \frac{1}{n^2}$$ by choosing $l=\Theta(\lg n)$ appropriately (e.g. $l>\lg n^6=6~\lg n \in \Theta(\lg n) $). Therefore it is ensured that by choosing $h_{a}$ uniformly at random, we will find an injective hash function in $\mathcal{O}(1)$ expected time with high probability. 

Note that at this point, we have reduced the problem of sorting $n$ $w$-bit integers to that of sorting $n$ $\Theta(q\lg n)$-bit integers.

We now use \textit{packed sorting} to sort the n length-reduced signatures $h_{a}(X_{1}),\dotsc,h_{a}(X_{n})$ in $\mathcal{O}(n)$ time (see fig. \ref{fig:psonhashes}). To be able to use this algorithm to sort $n$ $b$-bit integers, we have to ensure that $$b \leq \mathcal{O}\left(\frac{w}{\lg n \lg \lg n}\right).$$ In other words: it has to be possible to pack $\Omega(\lg n \lg \lg n)$ keys into one word $w$. This imposes our \textit{first} requirement for \textit{q}, since we want to sort signatures of length $b=\Theta(q\lg n)$.\newline

 \begin{figure}[h!btp]
 	\centering
 	\includegraphics[width=0.6\textwidth]{pics/fig3.pdf}
 	\caption[]{Using packed sorting to sort the signatures $h_{a}(X_{i})$. The result is the sorted sequence $Y_{1},\dotsc,Y_{n}$ with ($Y_{1} \leq \dotsc \leq Y_{n}$).}
 	\label{fig:psonhashes}
 \end{figure}


Let $Y_{1},\dotsc,Y_{n}$ now be the signatures in ascending order ($Y_{1} \leq \dotsc \leq Y_{n}$) after sorting (see figure \ref{fig:psonhashes}). Since our only requirement on $h_{a}$ was to be injective (and not neccesarily to be monotonic), signatures are now arranged in a differend order than that required by the original sorting problem. 
To solve this, we construct a trie (interpreting each hashed chunk as a character) by adding the sorted signatures $Y_{i}$ one-by-one from smallest to largest (see figure \ref{fig:bsp} for an example):

Assuming that we have already built a trie for $Y_{1},\dotsc,Y_{i-1}$, we add $Y_{i}$ as follows:%\vspace{-5mm}
\begin{itemize}
	\item At first we compute the \textit{longest common prefix} (LCP) of $Y_{i-1}$ and $Y_{i}$, which corresponds to the number of equal signature-chunks in $Y_{i-1}$ and $Y_{i}$, in $\mathcal{O}(1)$. This can be done for example by taking the most significant bit of $(Y_{i-1}~\textrm{XOR}~Y_{i})$.
	\item Then we walk up the rightmost path of the trie beginning at leaf $Y_{i-1}$ in order to find the insertion point of  $Y_{i}$. There are two possibilities:
\begin{enumerate}[label=(\alph*)]
	\item There already is a branching node at the LCP. Then $Y_{i}$ is simply added as one of its children (see fig. \ref{fig:trie} (a)).
	\item No branching node exists at the LCP position. In that case, we break up the edge after the LCP and add a new branching node. The remainder of that edge (which leads to the subtree that contains $Y_{i-1}$ as the rightmost leaf) as well as a new edge containing the different suffix of $Y_{i}$ are added as children to the new branching node (see fig. \ref{fig:trie} (b)). 
\end{enumerate}
\end{itemize}

 \begin{figure}[h!tbp]
 	\centering
 	\includegraphics[width=0.6\textwidth]{pics/fig4.pdf}
 	\caption[]{Inserting a new signature $Y_{i}$. (a) There already is a branching node at the LCP. Then $Y_{i}$ is simply added as one of its children. (b) A new branching node is added by breaking up an edge if it does not exist at the correct position. Both the subtree containing $Y_{i-1}$ as its rightmost leaf and $Y_{i}$ will be added as children of that node. The different suffixes will be used as edge lables.}
 	\label{fig:trie}
 \end{figure}

%Since we insert the $Y_{i}$'s in sorted order from small to large, only the rigthmost path is of interest when inserting the a signature. %Therefore already inserted keys will only be evaluated once. The walk to the branching node will decrease the length of this path if the suffix of $Y_{i-1}$ and  $Y_{i}$ differ in more than one chunk, because the length of the  \textit{new} rightmost path after inserting a suffix can only be increased by one. This is due the fact that we use the whole suffix (possibly consiting of several chunks) as edge label. In case the suffices of  $Y_{i-1}$ and  $Y_{i}$ only differ in the value of the last chuck, the length won't be reduced. 
It is sufficient to consider only the rightmost path when inserting a signature $Y_{i}$, because we add all of them in sorted order (small $\rightarrow$ large). Therefore, if  $LCP(Y_{i-1},Y_{i})>0$ the branching node (already existing or not) has to be on the rightmost path. In case of  $LCP(Y_{i-1},Y_{i})=0$ the new signature will be added as a new child of the root node.
The trie can be constructed in $\mathcal{O}(n)$ time, because we add every signature only once at a cost of $\mathcal{O}(1)$ for finding the correct insertion point and $\mathcal{O}(1)$ for adding it at that position.

Given this trie, we are now able to acutally sort the original input keys by sorting the edges of each internal node - using the original chunk values instead of the signature-labels as sort keys (see figure \ref{fig:bsp} (c) for an example). Because the order of an edge is determined by the first symbol on that edge, it is sufficient to use the first chunk as the sort key if an outgoing edge is labled with more than one chunk.

Thus our new sorting problem is to sort tuples of the form \textit{(nodeID,original chunk value, edge index)} - one for each edge in the trie. The edge index is used to keep track of the permutation so that we are able to permute the edges accordingly after sorting.

Because the trie contains $\mathcal{O}(n)$ edges, the reduced problem is to sort $\mathcal{O}(n)$  keys of size: 
$$\mathcal{O}\left(\lg n + \frac{w}{q} + \lg n\right)=\mathcal{O}\left(\frac{w}{q}\right) \textrm{, since } \lg n \in o(w) \textrm{ by our requirements on }w $$

To do so, we recurse on these $\mathcal{O}\left(\frac{w}{q}\right)$-bit keys, thereby successively reducing their length until it is possible to use \textit{packed sorting} as the base case. As we want the overall sorting time to be linear, we have to ensure that the recursion ends after $\mathcal{O}(1)$ steps. This imposes our \textit{second} requirement for $q$.

%To do so, we recurse on these  keys,  until the remaining sorting problem can be solved directly using \textit{packed sorting}. We have to ensure that we reach the base case after a constant number of recursion steps, since we want the overall sorting time to be linear.

%We want to have a total number of $\mathcal{O}\left(1+\frac{1}{\varepsilon}\right)$ recursion steps, until we are able to end the recursion by using \textit{packed sorting} as the base case. After these  $\mathcal{O}\left(1+\frac{1}{\varepsilon}\right)$ steps, the keys will have length $\mathcal{O}\left(\frac{w}{q^{1+\frac{1}{\varepsilon}}}\right)$, which has to be $\leq \textstyle \frac{w}{\lg n \lg \lg n}$ in order to use \textit{packed sorting}. This forms the second requirement on \textit{q}.

After the recursion we are able to sort the original edges of the trie according to the results of the recursive sort-call by scanning through the result-list of sorted edges and permutating the edges of the trie accodingly. Finally, the sorted sequence of the $n$ input keys can be read off the trie by a final left-to-right scan in $\mathcal{O}(n)$ time.\newline

It remains to be shown that \textit{q} can be defined such that it fulfills the imposed requirements:
\begin{enumerate}
	\item $q\lg n \leq \mathcal{O}\left(\frac{w}{\lg n \lg \lg n}\right)$ in order to be able to sort the signatures $h_{a}(X_{1}),\dotsc,h_{a}(X_{n})$ in $\mathcal{O}(n)$ time with packed sorting.
	%\item  $\mathcal{O}\left(\frac{w}{q^{1+\frac{1}{\varepsilon}}}\right) \leq \frac{w}{\lg n \lg \lg n} $ in order to use \textit{packed sorting} as the base case after $\mathcal{O}\left(1+\frac{1}{\varepsilon}\right)$ recursion steps.
	\item $\frac{w}{q^x} \leq \mathcal{O}\left(\frac{w}{\lg n \lg \lg n}\right)$: After $x$ recursion steps, the keys have to be small enough in order to be sorted with \textit{packed sorting} with $x=\mathcal{O}(1)$. 
\end{enumerate}

\begin{lem}
$q=\Theta\left(\frac{w}{\lg^2 n \lg \lg n}\right)=\Theta\left(\frac{\lg^{2+\varepsilon}n \lg \lg n}{\lg^2 n \lg \lg n}\right)=\Theta(\lg^\varepsilon n)$  fulfills the above mentioned requirements and leads to a length-reduction by a factor of $\Theta(\lg n \lg \lg n)$ in both cases.
\end{lem}
\begin{proof}
\textit{q} fulfills requirement 1:
\begin{align*}
	q\lg n &\leq \mathcal{O}\left(\frac{w}{\lg n \lg \lg n}\right) \\
	\Leftrightarrow \lg^{\varepsilon} n \lg n &\leq \mathcal{O}\left(\frac{\lg^{2+\varepsilon}n \lg \lg n}{\lg n \lg \lg n}\right)  \\
  	\Leftrightarrow \lg^{\varepsilon} n \lg n &\leq \mathcal{O}(\lg^{1+\varepsilon} n) \\
  	\Leftrightarrow \lg^{1+\varepsilon} n &\leq \mathcal{O}(\lg^{1+\varepsilon} n)
\end{align*}
\textit{q} fulfills requirement 2:
\begin{align*}
	\frac{w}{q^x} &= \Theta(\lg n \lg \lg n) \\
	\Leftrightarrow q^x &= \mathcal{O}\left(\frac{w}{\lg n \lg \lg n}\right)= \mathcal{O}\left(\frac{\lg^{2+\varepsilon}n \lg \lg n}{\lg n \lg \lg n}\right)= \mathcal{O}(\lg^{1+\varepsilon} n ) \\
	%\mathcal{O}(q^x) &= \mathcal{O}\left(\frac{\lg^{2+\varepsilon}n \lg \lg n}{\lg n \lg \lg n}\right)= \mathcal{O}(\lg^{1+\varepsilon} n )\\
	\Leftrightarrow x &= \mathcal{O}\left(\frac{\lg(\lg^{1+\varepsilon} n) }{\lg q}\right)=\mathcal{O}\left(\frac{\lg(\lg^{1+\varepsilon} n) }{\lg (\lg^\varepsilon n)}\right)=\mathcal{O}\left(\frac{1+\varepsilon}{\varepsilon}\right)\\
	\end{align*}
	 After $\mathcal{O}\left(1+\frac{1}{\varepsilon}\right)$ recursion steps, the keys are small enough to be sorted with packed sorting:
	 \begin{align*}
	\frac{w}{q^{1+\frac{1}{\varepsilon}}} &\leq  \mathcal{O}\left(\frac{w}{\lg n \lg \lg n}\right) \\
	\Leftrightarrow \frac{w}{(\lg^{\varepsilon}n)^{1+\frac{1}{\varepsilon}}} &\leq \mathcal{O}(\lg^{1+\varepsilon} n)\\
	%\mathcal{O}\left(\frac{w}{\lg^{1+\varepsilon}n}\right) &\leq\Omega(\lg^{1+\varepsilon} n) \\
	\Leftrightarrow \frac{\lg^{2+\varepsilon}n \lg \lg n}{\lg^{1+\varepsilon}n} &\leq \mathcal{O}(\lg^{1+\varepsilon} n) \\
	\Leftrightarrow \lg n \lg \lg n &\leq  \mathcal{O}(\lg^{1+\varepsilon} n)
\end{align*}
In both cases, a length-reduction by a factor of $\Theta(\lg n \lg \lg n)$ was sufficient to be able to use \textit{packed sorting} in order to sort in $\mathcal{O}(n)$ time:%\vspace{-5mm}
\begin{itemize}
	\item Using hashing to create signatures reduced the size of a key by a factor of $\Theta\left(\frac{\lg^{2+\varepsilon}n \lg \lg n}{\lg^{1+\varepsilon} n}\right)=\Theta(\lg n \lg \lg n)$
	\item After a total of $\mathcal{O}\left(1+\frac{1}{\varepsilon}\right)$ recursion steps, the size of a key is also reduced by a factor $\mathcal{O}\left(\frac{w}{q^{1+\frac{1}{\varepsilon}}}\right)=\Theta\left(\frac{\lg^{2+\varepsilon}n \lg \lg n}{\lg^{1+\varepsilon} n}\right)=\Theta(\lg n \lg \lg n)$
\end{itemize}
\end{proof}

To sum up, let us recall the different steps of signature sort:
\begin{enumerate}
	\item Partition each key into $q=\Theta(\lg^\varepsilon n)$ chunks of size $\frac{w}{q}=\Theta(\lg^2 n \lg \lg n)$.
	\item Create a signature of length $\Theta\left(q \cdot \lg n\right)$ for each key in $\mathcal{O}(n)$ time using an injective hash function $h_{a}$ chosen uniformly at random from the 2-universal class of hash functions $\mathcal{H}$. Since $\mathcal{H}_{k,l}$ is 2-universal, we are able to find such $h_{a}$ in $\mathcal{O}(1)$ expected time.
	\item Sort these signatures in $\mathcal{O}(n)$ time using \textit{packed sorting}.
	\item Build a trie on the sorted signatures in $\mathcal{O}(n)$ time.
	\item Recursivly sort the edges of that trie with the original chunk values as input keys. After $\mathcal{O}(1+\frac{1}{\varepsilon})$ recursion steps \textit{packed sorting} will be used as the base case.
	\item Permute the edges according to the results from the previous step in $\mathcal{O}(n)$.
	\item A left-to-right scan through the trie now yields the sorted sequence of the input keys and takes $\mathcal{O}(n)$ time.
\end{enumerate}

Therefore, if $w \geq  \lg^{2+\varepsilon}n \lg \lg n$ for some fixed $\varepsilon > 0$, we can sort $n$ $w$-bit integers in linear expeced time.

If we had chosen to recurse in the first place (on the chunks of the keys) the number of keys to recurse on would have been $n \cdot q$, while the ''trie technique'' was able to keep their number linear in $n$. By recursing on the original chunks, the running time would follow the recursion $$SORT(n,w)=SORT(nq,\frac{w}{q})=\mathcal{O}(n\cdot w),$$ which is even worse than radix sort!  

 \begin{figure}[h!btp]
 	\centering
 	\includegraphics[width=0.6\textwidth]{pics/bsp.pdf}
 	\caption[]{Example of signature sort steps: (a) Breaking keys into chunks and hash them to create signatures. (b) Use packed sorting to sort signatures. (c) Build trie based on sorted signatures. Grey boxes show the original chunk value corresponding to the signature chunk. (d) Recusivly sort edges of the tie using the original chunk values as sort keys. Afterwards, the sorted sequence of the input keys can be read off the trie.}
 	\label{fig:bsp}
 \end{figure}
% subsection signature_sort (end)

% section integer_sorting (end)

\bibliographystyle{plainnat}
%\label{lib}
\bibliography{lib}

\end{document}
